Steps: 

1. Open a bucket in your gcloud
2. Store the files and set to public (might be able to use Ninas after adding us, but is not sure yet) 
3. Pull the "api.js" in visual studio code (or wherever) and save the js file
4. Please adapt the code according to your audio (e.g., hertz and path of bucket) 
5. Apply the ReadMe_Commands.md on your terminal
6. press enter after each command. 
7. For the first command, adapt the path to where you stored your json file, you can keep the second command as it is, adapt the certain command to the path of your previously stored js code



Commands:

1. export GOOGLE_APPLICATION_CREDENTIALS=/Users/ninacorz/Documents/Podlyzer/podlyzer-speech-to-text-51e97e2d46a4.json

2. npm install --save @google-cloud/speech

3. npm install --save @google-cloud/storage

4. npm install --save csv-writer

5. node /Users/ninacorz/Developer/speech_to_text/longaudio.js
