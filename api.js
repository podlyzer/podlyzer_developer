// Imports the Google Cloud client library
const speech = require('@google-cloud/speech').v1p1beta1;
const {Storage} = require('@google-cloud/storage');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

//for getting file name
const { stdin, stdout } = process;
var aname ="";

function prompt(question) {
  return new Promise((resolve, reject) => {
    stdin.resume();
    stdout.write(question);

    stdin.on('data', data => resolve(data.toString().trim()));
    stdin.on('error', err => reject(err));
  });
}

async function main () {

// ask for file name
try{
  aname = await prompt("what is the file name?");
  stdin.pause();
}
catch(error) {
  console.log("There's an error!");
  console.log(error);
}

// Creates a speech client
const client = new speech.SpeechClient();

 const gcsUri = `gs://podlyzer/${aname}.mp3`;
 const encoding = 'LINEAR48';
 const sampleRateHertz = 48000;
 const languageCode = 'en-US';

const config = {
  encoding: encoding,
  sampleRateHertz: sampleRateHertz,
  languageCode: languageCode,
  enableAutomaticPunctuation: true,
  enableWordConfidence: true,
};

const audio = {
  uri: gcsUri,
};

const request = {
  config: config,
  audio: audio,
};

// Detects speech in the audio file. This creates a recognition job that you
// can wait for now, or get its result later.
const [operation] = await client.longRunningRecognize(request);
// Get a Promise representation of the final result of the job
const [response] = await operation.promise();
const transcription = response.results
  .map(result => result.alternatives[0].transcript)
  .join('\n');
// for showing confidence level results
const confidence = response.results
  .map(result => result.alternatives[0].confidence)
  .join('\n');
//console.log -> change to array
const csvWriter = createCsvWriter({
  path: `${aname}.csv`,
  header: [
    {id: 'transcription', title: 'Transcription'},
    {id: 'confidence', title: 'Confidence Level'},
  ]
});
const output =[
  {transcription: transcription,
    confidence: confidence
}];

csvWriter.writeRecords(output).then(()=>console.log (`${path} file created.`));

//for individual words level
//console.log('Word-Level-Confidence:');
//const words = response.results.map(result => result.alternatives[0]);
//words[0].words.forEach(a => {
  //console.log(` word: ${a.word}, confidence: ${a.confidence}`);
  //});
  
// for csv saving and upload
  const bucketName = 'transcriptspodlyzer';
  const filePath = `${aname}.csv`;
  const destFileName = `${aname}.csv`;

// save text into file
  fs.writeFile(filePath, output, (err) => {
      
    // In case of a error throw err.
    if (err) throw err;
})

  // Creates a storage client
  const storage = new Storage();
  const myBucket = storage.bucket(bucketName);
  const file = myBucket.file(destFileName);
  
  fs.createReadStream(filePath)
  .pipe(file.createWriteStream())
  .on('error', function(err) {})
  .on('finish', function() {
    //The file upload is complete.
    console.log(`${filePath} uploaded to ${bucketName} as ${destFileName}`);
  });

  // delete local file
  fs.unlink(filePath,function(err){
    console.log(`${filePath} deleted locally`);
    });

}
main().catch(console.error);